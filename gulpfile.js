var gulp 		 = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	minifycss 	 = require('gulp-minify-css'),
	notify 		 = require('gulp-notify'),
	browserSync  = require('browser-sync').create(),
	del 		 = require('del'),
	sass 		 = require('gulp-sass'),
	sftp 		 = require('gulp-sftp'),
	imagemin	 = require('gulp-imagemin'),
	pngquant     = require('imagemin-pngquant');

gulp.task('default',function() {
	browserSync.init({
        proxy: 'newspan.uk',
        browser: [ 'google chrome' ]
    });
	gulp.watch('wp-content/themes/ice/style.scss',['sass']);
	gulp.watch("wp-content/themes/ice/**/*.php").on('change', browserSync.reload);
});

gulp.task('sass', function() {
	del('wp-content/themes/ice/style.css');
	return gulp.src("wp-content/themes/ice/style.scss")
           .pipe(sass().on('error', sass.logError))
           .pipe(autoprefixer())
           .pipe(minifycss())
           .pipe(gulp.dest("wp-content/themes/ice"))
           .pipe(notify('SASS Built Successfully!'))
           .pipe(browserSync.stream());
});

gulp.task('images',function() {
	return gulp.src('wp-content/themes/ice/images/*')
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        interlaced: true,
        use: [pngquant()]
    }))
    .pipe(gulp.dest('wp-content/themes/ice/images'));
});