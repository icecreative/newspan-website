<header>
	<div class="top-bar">
		<div class="top-bar-inner">
			<div>
				<img class="site-logo" src="<?= img_dir() ?>/logo1.svg" alt="">
			</div>
			<div>
				<p>
					<a href="tel:02380269944">
						<span class="icon"><i class="fa fa-phone"></i></span><span class="hide-contact-mobile">02380 269944</span>
					</a>
				</p>
				<p>
					<a href="mailto:sales@newspan.co.uk">
						<span class="icon"><i class="fa fa-at"></i></span><span class="hide-contact-mobile">sales@newspan.co.uk</span>
					</a>
				</p>
			</div>
		</div>
	</div>
	<div class="main-menu">
		<div class="menu-container">
			<div class="mobile-menu-button">
				<i onclick="toggle_mobile_menu()" class="fa fa-bars"></i>
			</div>
			<?php wp_nav_menu(array(
				'theme_location' => 'header-menu',
				'container' => false
			)); ?>
			<div class="site-search">
				<?php get_search_form() ?>
				<div onclick="search_site(this)" class="search-button">
					<i class="fa fa-search"></i>
				</div>
			</div>
		</div>
	</div>
	<?php if(get_field('header_image')) : ?>
		<?php 
			$header_image = get_field('header_image');
		?>
		<div class="header-image" style="background-image: url('<?php echo $header_image['url']; ?>')">
			<?php get_template_part('partials/consultation-service'); ?>
		</div>
		<?php get_template_part('partials/mobile-consultation-service'); ?>
	<?php else : ?>
		<div class="header-image" style="background-image: url(<?= img_dir(); ?>/home-header.jpg)">
			<?php get_template_part('partials/consultation-service'); ?>
		</div>
		<?php get_template_part('partials/mobile-consultation-service'); ?>
	<?php endif; ?>
</header>