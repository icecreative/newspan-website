<div class="content-constrainer main-content sustainable-building-row">

	<h1><?php the_field('bottom_section_title') ?></h1>
	<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
	<?php the_field('bottom_title_content') ?>

	<?php
		$query = new WP_Query( array( 'post_type' => 'services', 'posts_per_page' => -1 ) );
	?>
	
	<div class="clear service-icons">
		<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

			<div class="service-icon">
				<div class="service-image">
					<img src="<?= get_field('service_icon')['url'] ?>" alt="<?= get_field('service_icon')['alt'] ?>">
				</div>
				<div class="button-container">
					<a href="/services#<?= str_replace(' ', '-', strtolower(get_the_title())) ?>" class="btn"><?php the_title() ?></a>
				</div>
			</div>

		<?php endwhile; wp_reset_postdata(); ?>
	</div>

<?php else : ?>
	<p><?php _e( 'Sorry, no staff can be found at this time.' ); ?></p>
<?php endif; ?>


</div>