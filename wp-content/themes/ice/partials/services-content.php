<?php
	$query = new WP_Query( array( 'post_type' => 'services', 'posts_per_page' => -1, 'order' => 'ASC' ) );
	$i = 0;
?>
<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

	<?php 
		if($i % 2 !== 0)
		{
			$class = 'odd';
		}
		else
		{
			$class = 'even';
		}
		$i++;
	?>

	<div class="service <?= $class ?>" id="<?= strtolower(str_replace(' ', '-',  get_the_title())) ?>">
		
		<div class="service-title">
			<div class="service-image">
				<img src="<?= get_field('service_icon')['url'] ?>" alt="<?= get_field('service_icon')['alt'] ?>">
			</div>
			<h1><?php the_title() ?></h1>
			<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
		</div>

		<div class="service-content clear">
			<div class="service-background service-height" style="background-image: url('<?= get_field('service_image')['url'] ?>');">
			</div>
			<div class="service-height service-text">
				<div class="service-text-inner">
					<h2><?php the_field('content_title') ?></h2>
					<?php the_field('content') ?>
				</div>
			</div>
		</div>

	</div>

<?php endwhile; wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php _e( 'Sorry, no services can be found at this time.' ); ?></p>
<?php endif; ?>