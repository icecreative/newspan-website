<div class="sector-icons">
	<div class="content-constrainer clear">
		<div class="sector-icon">
		<img src="<?= img_dir() ?>/Garden-Centre.svg" alt="">
			<a href="/what-we-do" class="btn">GARDEN CENTRE</a>
		</div>
		<div class="sector-icon">
		<img src="<?= img_dir() ?>/High-Street.svg" alt="">
			<a href="/what-we-do" class="btn">HIGH STREET</a>
		</div>
		<div class="sector-icon">
		<img src="<?= img_dir() ?>/Public-Sector.svg" alt="">
			<a href="/what-we-do" class="btn">PUBLIC SECTOR</a>
		</div>
	</div>
</div>