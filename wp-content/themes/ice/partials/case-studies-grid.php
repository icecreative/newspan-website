<div class="case-studies-grid">

	<div class="clear">
		<?php
			if(is_page(30))
			{
				$post_amount = -1;
			}
			else
			{
				$post_amount = 3;
			}
			$car_query = new WP_Query( array( 'post_type' => 'case_studies', 'posts_per_page' => $post_amount, 'orderby' => 'rand' ) );
		?>
		<?php if ($car_query->have_posts()) : while ($car_query->have_posts()) : $car_query->the_post(); ?>
			
			<?php
				$image = get_field('carousel_images')[0]['image'];
			?>

			<div class="case-study" style="background-image: url(<?= $image['url'] ?>);">
				<a href="<?php the_permalink() ?>">
					<div class="overlay">
						<h1><?php the_title() ?></h1>
					</div>
				</a>
				<div class="button">
					<a class="btn" href="<?php the_permalink() ?>">VIEW CASE STUDY</a>
				</div>
			</div>

		<?php endwhile; wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no services can be found at this time.' ); ?></p>
		<?php endif; ?>
	</div>
</div>