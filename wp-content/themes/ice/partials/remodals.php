<div class="remodal" data-remodal-id="mobile-search-form">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Search Newspan</h1>
	<?php get_search_form() ?>
</div>

<div class="remodal" data-remodal-id="mobile-map-item">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1 class="title"></h1>
	<p class="content"></p>
</div>