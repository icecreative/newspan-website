<div class="case-studies-grid related-case-studies">

	<div class="content-constrainer main-content page-header">
        <h1>RELATED CASE STUDIES</h1>
        <img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
    </div>


	<div class="clear">
		<?php
			$car_query = new WP_Query( array( 'post_type' => 'case_studies', 'cat' => get_the_category()[0]->term_id, 'posts_per_page' => 3 ) );
		?>
		<?php if ($car_query->have_posts()) : while ($car_query->have_posts()) : $car_query->the_post(); ?>
			
			<?php
				$image = get_field('carousel_images')[0]['image'];
			?>

			<div class="case-study" style="background-image: url(<?= $image['url'] ?>);">
				<a href="<?php the_permalink() ?>">
					<div class="overlay">
						<h1><?php the_title() ?></h1>
					</div>
				</a>
				<div class="button">
					<a class="btn" href="<?php the_permalink() ?>">VIEW CASE STUDY</a>
				</div>
			</div>

		<?php endwhile; wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no services can be found at this time.' ); ?></p>
		<?php endif; ?>
	</div>
</div>