<?php
	$query = new WP_Query( array( 'post_type' => 'sectors', 'posts_per_page' => -1 ) );
	$i = 0;
?>

<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

	<?php 
		if($i % 2 !== 0)
		{
			$class = 'odd';
		}
		else
		{
			$class = 'even';
		}
		$i++;
	?>

	<div class="service <?= $class ?>" id="<?= strtolower(str_replace(' ', '-',  get_the_title())) ?>">
		
		<div class="service-title">
			<div class="service-image">
				<img src="<?= get_field('sector_icon')['url'] ?>" alt="<?= get_field('sector_icon')['alt'] ?>">
			</div>
			<h1><?php the_title() ?></h1>
			<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
		</div>

		<div class="service-content clear">
			<div class="service-background sector-carousel-container service-height">
				<div class="sector-carousel">
					<div class="carousel">
						<?php
							$car_query = new WP_Query( array( 'post_type' => 'case_Studies', 'category_name' => strtolower(str_replace(' ', '-',  get_the_title())), 'posts_per_page' => -1 ) );
						?>
						<?php if ($car_query->have_posts()) : while ($car_query->have_posts()) : $car_query->the_post(); ?>
							
							<?php
								$image = get_field('carousel_images')[0]['image'];
							?>

							<div>
								<div class="carousel-item service-height" style="background-image: url(<?= $image['url'] ?>)">
									<div class="carousel-content">
										<h2><?php the_title() ?></h2>
										<span><?php the_field('carousel_text') ?></span>
										<a class="btn secondary" href="<?php the_permalink() ?>">Read more</a>
									</div>
								</div>
							</div>

						<?php endwhile; $query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no services can be found at this time.' ); ?></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="service-height service-text sector-content">
				<div class="service-text-inner">
					<h2><?php the_field('content_title') ?></h2>
					<?php the_field('content') ?>
				</div>
			</div>
		</div>

		<div class="service-grey-section">
			<div class="service-grey-section-inner">
				<?php the_field('grey_section') ?>
			</div>
		</div>

	</div>

<?php endwhile; wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php _e( 'Sorry, no services can be found at this time.' ); ?></p>
<?php endif; ?>