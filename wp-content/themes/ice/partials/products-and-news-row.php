<div class="products-and-news-row">
	<div class="content-constrainer clear">
		<div class="featured-projects match-height">
			<div class="title">
				<h1>Featured Projects</h1>
				<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
			</div>
			<div class="featured-carousel">
				<?php
					$car_query = new WP_Query( array( 'post_type' => 'case_Studies', 'posts_per_page' => -1 ) );
				?>
				<?php if ($car_query->have_posts()) : while ($car_query->have_posts()) : $car_query->the_post(); ?>
					
					<?php
						$image = get_field('carousel_images')[0]['image'];
					?>

					<div>
						<div class="image" style="background-image: url(<?= $image['url'] ?>);"></div>
						<div class="content">
							<h2><?php the_title() ?></h2>
							<p><?= substr(get_field('content'), 0, 700) . '...' ?></p>
							<a href="<?php the_permalink() ?>" class="btn">READ MORE</a>
						</div>
					</div>
						
					

				<?php endwhile; wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php _e( 'Sorry, no services can be found at this time.' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
		<div class="latest-news match-height">
			<div class="title">
				<h1>Latest News</h1>
				<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
				<?php show_latest_news() ?>
				<a class="read-more-news btn" href="/news">Read more</a>
			</div>
		</div>
	</div>
</div>