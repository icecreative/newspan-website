<?php the_post_thumbnail() ?>

<table width="100%">
	<tr>
		<td> <?php display_post_category_icon() ?></td>
		<td class="title"><?php the_title() ?></td>
	</tr>
</table>

<h2>Posted on <?php the_date() ?> by <?php the_author() ?></h2>

<div class="post-content">
	<?php the_content() ?>	
</div>

<div class="post-buttons">
	<div class="share-posts">
		<a href="" class="btn">Share Post</a>
		<div class="social-icons">
			<a class="social-icon facebook" href="javascript: void(0)" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(get_the_permalink()); ?>', 'sharer', 'width=520, height=350')">
				<i class="fa fa-facebook"></i>
			</a>
			<a class="social-icon twitter" href="javascript: void(0)" onclick="window.open('https://twitter.com/home?status=<?= urlencode(get_the_title()); ?>%20-%20<?= urlencode(get_the_permalink()); ?>', 'sharer', 'width=520, height=350')">
				<i class="fa fa-twitter"></i>
			</a>
			<a class="social-icon linkedin" href="javascript: void(0)" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=<?= urlencode(get_the_permalink()); ?>&title=<?= urlencode(get_the_title()); ?>&summary=<?= urlencode(strip_tags(get_the_excerpt())); ?>', 'sharer', 'width=520, height=350')">
				<i class="fa fa-linkedin"></i>
			</a>
			<a class="social-icon pinterest" href="javascript: void(0)" onclick="window.open('https://pinterest.com/pin/create/button/?url=<?= urlencode(get_the_permalink()); ?>&media=<?= urlencode(get_the_post_thumbnail_url()); ?>&description=<?= urlencode(get_the_title()); ?>', 'sharer', 'width=520, height=350')">
				<i class="fa fa-pinterest"></i>
			</a>
			<a class="social-icon google" href="javascript: void(0)" onclick="window.open('https://plus.google.com/share?url=<?= urlencode(get_the_permalink()); ?>', 'sharer', 'width=520, height=350')">
				<i class="fa fa-google-plus"></i>
			</a>
		</div>
	</div>
	<script>
	    document.write('<a class="btn" href="' + document.referrer + '">Go Back</a>');
	</script>
</div>