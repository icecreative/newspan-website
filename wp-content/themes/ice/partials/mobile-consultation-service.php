<div class="mobile-consultation-service consultation-service">
	<div class="consultation-title" onclick="show_mobile_consultation(this)">
		<h1>FREE CONSULTATION &amp; DESIGN SERVICE</h1>
	</div>
	<div class="consultation-content">
		<div class="icon-image">
			<img src="<?= img_dir() ?>/icon-image-two.png" alt="">
		</div>
		<p>If you’d like to arrange a meeting with us regarding our free consultation and design service, please enter your details and choose a suitable date below and we’ll get back to you to confirm.</p>
		<script>var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>"</script>
		<form id="consultation-form" onsubmit="submit_consultation_form(event, this)">
			<p class="message"></p>
			<input type="hidden" name="action" class="action" value="send_consultation_form">
			<input type="hidden" name="date" class="date" value="">
			<input type="text" name="name" placeholder="Name">
			<input type="text" name="company" placeholder="Company Name">
			<input type="tel" name="tel" placeholder="Phone Number">
			<input type="email" name="email" placeholder="Email Address">
			<span class="datepicker-label">Suggest a Date:</span>
			<span class="datepicker-date">dd/mm/yyyy</span>
			<div name="date" id="datepicker-mobile"></div>
			<input type="submit" class="btn" value="Submit">
		</form>
	</div>
</div>