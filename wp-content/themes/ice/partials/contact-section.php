<div class="content-constrainer contact-section clear">
	<div class="contact-details">
		<table width="100%">
			<tr>
				<td>
					<div class="icon">
						<i class="fa fa-home"></i>
					</div>
				</td>
				<td>
					<?php the_field('address', 247) ?>
				</td>
			</tr>
			<tr>
				<td>
					<div class="icon">
						<i class="fa fa-phone"></i>
					</div>
				</td>
				<td>
					<a href="tel:<?php the_field('telephone', 247) ?>"><?php the_field('telephone', 247) ?></a>
				</td>
			</tr>
			<tr>
				<td>
					<div class="icon">
						<i class="fa fa-at"></i>
					</div>
				</td>
				<td>
					<a href="mailto:<?php the_field('email', 247) ?>"><?php the_field('email', 247) ?></a>
				</td>
			</tr>
			<tr>
				<td>
					<div class="icon">
						<i class="fa fa-facebook"></i>
					</div>
				</td>
				<td>
					<a target="_blank" href="http://facebook.com/<?php the_field('facebook', 247) ?>"><?php the_field('facebook', 247) ?></a>
				</td>
			</tr>
			<tr>
				<td>
					<div class="icon">
						<i class="fa fa-twitter"></i>
					</div>
				</td>
				<td>
					<a target="_blank" href="http://twitter.com/"<?php the_field('twitter', 247) ?>><?php the_field('twitter', 247) ?></a>
				</td>
			</tr>
		</table>
	</div>
	<div class="contact-form">
		<?= do_shortcode('[contact-form-7 id="253" title="Contact Page Form"]') ?>
	</div>
</div>