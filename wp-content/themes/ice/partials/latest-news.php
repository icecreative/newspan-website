<div class="latest-post">
	<div class="post-content">
		<table width="100%">
			<tr>
				<td>&nbsp;</td>
				<td class="date"><?php the_date() ?></td>
			</tr>
			<tr>
				<td> <?php display_post_category_icon() ?></td>
				<td class="title"><?php the_title() ?></td>
			</tr>
		</table>
		<?php the_excerpt() ?>
	</div>
</div>
<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">