<div class="search">
	<h1>Search Posts</h1>
	<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
	<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	    <div class="field">
	    	<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
	    	<span onclick="submit_form(this)" class="search-submit"><i class="fa fa-search"></i></span>
	    </div>
	    <input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
	</form>
</div>

<div class="filter">
	<h1>Filter Posts</h1>
	<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
	<table>
		<tr>
			<td>Service:</td>
			<td><?= filter_sub_categories('services') ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Sector:</td>
			<td><?= filter_sub_categories('sectors') ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Date:</td>
			<td>
				<select>
					<option value="">Select...</option>
					<?php wp_get_archives($args = array(
						'format' => 'option'
					)) ?>
				</select>
			</td>
		</tr>
	</table>
</div>

<div class="facebook">
	<h1>Facebook</h1>
	<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
	<div class="facebook-posts"></div>
	<a href="https://www.facebook.com/newspan" target="_blank">Read more...</a>
</div>

<!-- <div class="twitter">
	<h1>Twitter</h1>
	<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
	<div id="tweets" class="twitter-posts"></div>
</div> -->