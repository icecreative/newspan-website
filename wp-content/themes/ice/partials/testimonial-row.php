<div class="testimonial-row" style="background-image: url(<?= img_dir() ?>/testimonial-background.png);">
	<div class="title">
		<h1>Testimonials</h1>
		<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
		<div class="testimonial-carousel">
			<div>
				<div class="content-constrainer">
					<div class="testimonial-item">
						<h2>number 1</h2>
						<p>“As well as Blue Diamond’s previous experience of working with Newspan, we really liked their approach too. Their estimate was competitive, and I had confidence that they thoroughly understood the concept behind the design and wouldn’t compromise it in the delivery. They’ve also been pro-active in helping us to find solutions that improve not only the building’s aesthetics, but also its performance, particularly with ideas for increasing energy efficiency and reducing operating costs for heating and ventilation.”</p>
					</div>
				</div>
			</div>
			<div>
				<div class="content-constrainer">
					<div class="testimonial-item">
						<h2>number 2</h2>
						<p>“As well as Blue Diamond’s previous experience of working with Newspan, we really liked their approach too. Their estimate was competitive, and I had confidence that they thoroughly understood the concept behind the design and wouldn’t compromise it in the delivery. They’ve also been pro-active in helping us to find solutions that improve not only the building’s aesthetics, but also its performance, particularly with ideas for increasing energy efficiency and reducing operating costs for heating and ventilation.”</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>