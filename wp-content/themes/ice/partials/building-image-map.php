<div class="building-image-map">
	<img class="building-image" src="<?= img_dir() ?>/background-home-markers.jpg" alt="">
	<div class="map-item" id="item-one">
		<div class="map-marker"></div>
		<div class="map-item-content">
			<div class="title">
				<h1>SURVEY</h1>
			</div>
			<div class="content">
				<p>With all projects Newspan starts with a survey to determine what work is necessary, and what relevant expertise the company will be best placed to provide.</p>
				<a href="/how-we-do-it/" onclick="forceClick(this)" class="btn secondary">READ MORE</a>
			</div>
		</div>
	</div>
	<div class="map-item" id="item-two">
		<div class="map-marker"></div>
		<div class="map-item-content">
			<div class="title">
				<h1>DESIGN</h1>
			</div>
			<div class="content">
				<p>Once the survey is complete, Newspan's CAD trained designers will render mock-ups of the project to start the discussion as to what the client needs and how best the Newspan team can deliver it.</p>
				<a href="/how-we-do-it/" onclick="forceClick(this)" class="btn secondary">READ MORE</a>
			</div>
		</div>
	</div>
	<div class="map-item" id="item-three">
		<div class="map-marker"></div>
		<div class="map-item-content">
			<div class="title">
				<h1>PROJECT FULFILMENT</h1>
			</div>
			<div class="content">
				<p>The aim of the Newspan team is to work alongside the client to ensure the final project reflects the needs of the client and any potential customers. Newspan will ensure the project isn’t signed off until the client is 100% happy.</p>
				<a href="/how-we-do-it/" onclick="forceClick(this)" class="btn secondary">READ MORE</a>
			</div>
		</div>
	</div>
	<div class="map-item" id="item-four">
		<div class="map-marker"></div>
		<div class="map-item-content">
			<div class="title">
				<h1>MATERIALS</h1>
			</div>
			<div class="content">
				<p>Newspan is always on the lookout to improve its service and that includes sourcing the best materials for every job. Quality, environmental impact and cost are all factors the team consider.</p>
				<a href="/how-we-do-it/" onclick="forceClick(this)" class="btn secondary">READ MORE</a>
			</div>
		</div>
	</div>
</div>

<div class="services-button">
	<a href="/our-work" class="btn primary">View our work</a>
</div>