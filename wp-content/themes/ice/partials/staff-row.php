<?php
$query = new WP_Query( array( 'post_type' => 'staff_member', 'posts_per_page' => -1 ) );
?>

<?php if ( $query->have_posts() ) : ?>

	<div class="staff-carousel">
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<div>
				<div class="staff-member">
					<div class="title-section">
						<h1><?php the_field('job_title') ?></h1>
						<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
					</div>
					<img src="<?= get_field('image')['url'] ?>" alt="<?= get_field('image')['alt'] ?>">
					<p><span><?php the_title() ?></span> <?php the_field('description') ?></p>
				</div>
			</div>	
		<?php endwhile; ?>
	</div>

	<?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php _e( 'Sorry, no staff can be found at this time.' ); ?></p>
<?php endif; ?>

