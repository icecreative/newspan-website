<div class="post <?= add_first_post_class() ?>">
	<a href="<?php the_permalink() ?>">
		<div class="post-image" style="background-image: url(<?php the_post_thumbnail_url() ?>)"></div>
	</a>
	<div class="post-content">
		<table width="100%">
			<tr>
				<td>&nbsp;</td>
				<td class="date"><?php the_date() ?></td>
			</tr>
			<tr>
				<td> <?php display_post_category_icon() ?></td>
				<td class="title"><?php the_title() ?></td>
			</tr>
		</table>
		<?php the_excerpt() ?>
	</div>
</div>

