<?php
	$query = new WP_Query( array( 'post_type' => 'projects', 'posts_per_page' => -1 ) );
	$i = 0;
?>
<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

	<?php 
		if($i % 2 !== 0)
		{
			$class = 'odd';
		}
		else
		{
			$class = 'even';
		}
		$i++;
	?>

	<div class="project-section carousel-container">
		<span class="title"><?php the_title() ?></span>
		<div class="project-carousel <?= $class ?>">
			
			<?php if(have_rows('project_type')) : while(have_rows('project_type')) : the_row(); ?>
				<div>
					<div class="content clear" id="<?php the_permalink() ?>" style="background-image: url(<?= get_sub_field('background_image')['url'] ?>)">
						<div class="text">
							<h2><?php the_sub_field('title') ?></h2>
							<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
							<?php the_sub_field('content') ?>
						</div>
					</div>
				</div>
			<?php endwhile; endif; ?>

		</div>
	</div>

<?php endwhile; wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php _e( 'Sorry, no services can be found at this time.' ); ?></p>
<?php endif; ?>