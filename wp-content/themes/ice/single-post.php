<?php
    require('header.php');
?>

<div class="post-page single-post-page">

	<div class="content-constrainer clear">
		<div class="posts single-post">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('partials/single-post-content') ?>
			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, this page is currently unavailable.' ); ?></p>
			<?php endif; ?>
		</div>
		<div class="sidebar">
			<?php get_template_part('partials/post-sidebar') ?>
		</div>
	</div>
</div>

<?php
    require('footer.php');
?>