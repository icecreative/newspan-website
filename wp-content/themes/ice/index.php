<?php
    require('header.php');
?>

<div class="post-page">
	<?php if(is_paged()) : ?>
		<div class="content-constrainer">
			<div class="load-more load-previous">
				<a onclick="load_more_posts(event, this)" href="" class="btn" data-prev="1" data-page="<?= post_page_number(1) ?>" data-url="<?= admin_url('admin-ajax.php') ?>">
					<span class="ajax-loader"><i class="fa fa-refresh"></i></span>
					<span class="button-text previous">LOAD PREVIOUS</span>
				</a>
			</div>
		</div>
	<?php endif; ?>
	<div class="content-constrainer clear">
		<div class="posts">
			<?php if ( have_posts() ) : $i = 0; ?>
				<div class="page-limit clear" data-page="/news/<?= post_page_number() ?>">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php include(locate_template('partials/post-list.php')) ?>
						<?php $i++; ?>
					<?php endwhile; ?> 
				</div>
			<?php else : ?>
				<p><?php _e( 'Sorry, this page is currently unavailable.' ); ?></p>
			<?php endif; ?>
		</div>
		<div class="load-more mobile">
			<a onclick="load_more_posts(event, this)" href="" class="btn" data-page="1" data-url="<?= admin_url('admin-ajax.php') ?>">
				<span class="ajax-loader"><i class="fa fa-refresh"></i></span>
				<span class="button-text">LOAD MORE</span>
			</a>
		</div>
		<div class="sidebar">
			<?php get_template_part('partials/post-sidebar') ?>
		</div>
	</div>
	<div class="content-constrainer">
		<div class="load-more desktop">
			<a onclick="load_more_posts(event, this)" href="" class="btn" data-page="<?= post_page_number(1) ?>" data-url="<?= admin_url('admin-ajax.php') ?>">
				<span class="ajax-loader"><i class="fa fa-refresh"></i></span>
				<span class="button-text">LOAD MORE</span>
			</a>
		</div>
	</div>
</div>

<?php
    require('footer.php');
?>


