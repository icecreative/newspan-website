<?php
    require('header.php');
?>

<div class="post-page single-post-page single-case-study-page" id="anchor">

	<div class="content-constrainer">
		<div class="single-case-study-carousel clear">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php if(have_rows('carousel_images')) : ?>
					<div class="slider-for">
						<?php while(have_rows('carousel_images')) : the_row('carousel_images'); ?>
							<div>
								<div class="image">
									<img src="<?= get_sub_field('image')['url'] ?>" alt="">
								</div>
							</div>
						<?php endwhile; ?>
					</div>
					<div class="slider-nav">
						<?php while(have_rows('carousel_images')) : the_row('carousel_images'); ?>
							<div>
								<div class="image">
									<img src="<?= get_sub_field('image')['url'] ?>" alt="">
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, this page is currently unavailable.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>

	<div class="mobile-case-study-carousel">
		<div class="slides">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php if(have_rows('carousel_images')) : ?>
						<?php $i = 1; ?>
						<?php while(have_rows('carousel_images')) : the_row('carousel_images'); ?>
							<div class="image <?php if($i == 1): ?> active <?php endif; ?>">
								<img src="<?= get_sub_field('image')['url'] ?>" alt="">
							</div>
						<?php $i++; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else : ?>
				<p><?php _e( 'Sorry, this page is currently unavailable.' ); ?></p>
			<?php endif; ?>
		</div>
		<div class="arrows">
			<div class="arrow left-arrow" onclick="caseStudyArrowClick(this)">
				<i class="fa fa-chevron-left"></i>
			</div>
			<div class="arrow right-arrow" onclick="caseStudyArrowClick(this)">
				<i class="fa fa-chevron-right"></i>
			</div>
		</div>
	</div>

	<div class="clear">
		<div class="content-constrainer clear">
			<div class="posts single-post">

				<table width="100%">
					<tr>
						<td> <?php display_post_category_icon() ?></td>
						<td class="title"><?php the_title() ?></td>
					</tr>
				</table>

				<div class="post-content">
					<?php the_field('content') ?>
				</div>

				<div class="post-buttons">
					<div class="share-posts">
						<a href="" class="btn">Share Post</a>
						<div class="social-icons">
							<a class="social-icon facebook" href="javascript: void(0)" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(get_the_permalink()); ?>', 'sharer', 'width=520, height=350')">
								<i class="fa fa-facebook"></i>
							</a>
							<a class="social-icon twitter" href="javascript: void(0)" onclick="window.open('https://twitter.com/home?status=<?= urlencode(get_the_title()); ?>%20-%20<?= urlencode(get_the_permalink()); ?>', 'sharer', 'width=520, height=350')">
								<i class="fa fa-twitter"></i>
							</a>
							<a class="social-icon linkedin" href="javascript: void(0)" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=<?= urlencode(get_the_permalink()); ?>&title=<?= urlencode(get_the_title()); ?>&summary=<?= urlencode(strip_tags(get_the_excerpt())); ?>', 'sharer', 'width=520, height=350')">
								<i class="fa fa-linkedin"></i>
							</a>
							<a class="social-icon pinterest" href="javascript: void(0)" onclick="window.open('https://pinterest.com/pin/create/button/?url=<?= urlencode(get_the_permalink()); ?>&media=<?= urlencode(get_the_post_thumbnail_url()); ?>&description=<?= urlencode(get_the_title()); ?>', 'sharer', 'width=520, height=350')">
								<i class="fa fa-pinterest"></i>
							</a>
							<a class="social-icon google" href="javascript: void(0)" onclick="window.open('https://plus.google.com/share?url=<?= urlencode(get_the_permalink()); ?>', 'sharer', 'width=520, height=350')">
								<i class="fa fa-google-plus"></i>
							</a>
						</div>
					</div>
					<script>
					    document.write('<a class="btn" href="' + document.referrer + '">Back to projects</a>');
					</script>
				</div>

			</div>
			<div class="sidebar">
				<?php if(get_field('sidebar_title')) : ?>
					<h1><?php the_field('sidebar_title') ?></h1>
					<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
				<?php endif; ?>
				<?php if(get_field('sidebar_content')) : ?>
					<p><?php the_field('sidebar_content') ?></p>
				<?php endif; ?>
				<?php if(get_field('client')) : ?>
					<p><span>Client: </span><?php the_field('client') ?></p>
				<?php endif; ?>
				<?php if(get_field('builder')) : ?>
					<p><span>Builder: </span><?php the_field('builder') ?></p>
				<?php endif; ?>
				<?php if(get_field('architect')) : ?>
					<p><span>Architect: </span><?php the_field('architect') ?></p>
				<?php endif; ?>
				<?php if(get_field('services_provided')) : ?>
					<p><span>Services Provided: </span><?php the_field('services_provided') ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div style="clear:both"></div>

	<?php get_template_part('partials/related-case-studies') ?>

</div>

<?php
    require('footer.php');
?>