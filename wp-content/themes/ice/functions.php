<?php

function theme_dir()
{
	return '/wp-content/themes/ice';
}

function img_dir()
{
	return theme_dir().'/images';
}

function components_dir()
{
	return '/bower_components';
}

function register_main_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_main_menu' );

add_theme_support( 'post-thumbnails' );


// Prevent empty search from going to post page
if(!is_admin()){
    add_action('init', 'search_query_fix');
    function search_query_fix(){
        if(isset($_GET['s']) && $_GET['s']==''){
            $_GET['s']=' ';
        }
    }
}

// Add SVG support to wordpress
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// Show SVG in backend
function custom_admin_head() {
  $css = '';

  $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';

  echo '<style type="text/css">'.$css.'</style>';
}
add_action('admin_head', 'custom_admin_head');


// Custom Excerpt with HTML allowed
function wpse_allowedtags() {
    return '<script>,<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>'; 
}
add_action( 'init', 'sk_add_category_taxonomy_to_events' );
function sk_add_category_taxonomy_to_events() {
    register_taxonomy_for_object_type( 'category', 'case_studies' );
}

// Register contact details
register_post_type( 'contact_details', array(
    'labels' => array(
        'name' => 'Contact Details',
        'singular_name' => 'Contact Detail'
    ),
    'description' => 'Newspan Contact Details',
    'public' => true,
    'menu_icon' => 'dashicons-businessman',
    'menu_position' => 10,
    'supports' => array('title', 'custom-fields')
));
// Register projects
register_post_type( 'projects', array(
    'labels' => array(
        'name' => 'Projects',
        'singular_name' => 'Project'
    ),
    'description' => 'Projects at Newspan',
    'public' => true,
    'menu_icon' => 'dashicons-businessman',
    'menu_position' => 9,
    'supports' => array('title', 'custom-fields')
));
// Register case studie
register_post_type( 'case_studies', array(
    'labels' => array(
        'name' => 'Case Studies',
        'singular_name' => 'Case Study'
    ),
    'description' => 'Case Studies at Newspan',
    'public' => true,
    'menu_icon' => 'dashicons-businessman',
    'menu_position' => 8,
    'supports' => array('title', 'custom-fields')
));

// Register sectors post type
register_post_type( 'sectors', array(
    'labels' => array(
        'name' => 'Sectors',
        'singular_name' => 'Sector'
    ),
    'description' => 'Sectors at Newspan',
    'public' => true,
    'menu_icon' => 'dashicons-businessman',
    'menu_position' => 7,
    'supports' => array('title', 'custom-fields')
));

// Register staff carousel
register_post_type( 'staff_member', array(
    'labels' => array(
        'name' => 'Staff Members',
        'singular_name' => 'Staff Member'
    ),
    'description' => 'Staff members at Newspan',
    'public' => true,
    'menu_icon' => 'dashicons-businessman',
    'menu_position' => 6,
    'supports' => array('title', 'custom-fields')
));

// Register services
register_post_type( 'services', array(
    'labels' => array(
        'name' => 'Services',
        'singular_name' => 'Service'
    ),
    'description' => 'Services at Newspan',
    'public' => true,
    'menu_icon' => 'dashicons-hammer',
    'menu_position' => 5,
    'supports' => array('title', 'custom-fields')
));

if ( ! function_exists( 'wpse_custom_wp_trim_excerpt' ) ) : 

    function wpse_custom_wp_trim_excerpt($wpse_excerpt) {
    $raw_excerpt = $wpse_excerpt;
        if ( '' == $wpse_excerpt ) {

            $wpse_excerpt = get_the_content('');
            $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
            $wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

            //Set the excerpt word count and only break after sentence is complete.
                if(is_front_page())
                {
                    $excerpt_word_count = 20;
                }
                else
                {
                    $excerpt_word_count = 60;
                }
                
                $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
                $tokens = array();
                $excerptOutput = '';
                $count = 0;

                // Divide the string into tokens; HTML tags, or words, followed by any whitespace
                preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

                foreach ($tokens[0] as $token) { 

                    if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) { 
                    // Limit reached, continue until , ; ? . or ! occur at the end
                        $excerptOutput .= trim($token);
                        break;
                    }

                    // Add words to complete sentence
                    $count++;

                    // Append what's left of the token
                    $excerptOutput .= $token;
                }

            $wpse_excerpt = trim(force_balance_tags($excerptOutput));

                $excerpt_end = ' <a class="btn" href="'. esc_url( get_permalink() ) . '">READ MORE</a>'; 
                $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end); 

                //$pos = strrpos($wpse_excerpt, '</');
                //if ($pos !== false)
                // Inside last HTML tag
                //$wpse_excerpt = substr_replace($wpse_excerpt, $excerpt_end, $pos, 0); /* Add read more next to last word */
                //else
                // After the content
                $wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */

            return $wpse_excerpt;   

        }
        return apply_filters('wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
    }

endif; 

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'wpse_custom_wp_trim_excerpt');

function filter_sub_categories($parent)
{

	if($parent == 'services')
	{
		$args = array('child_of' => 25);
	}
	elseif($parent == 'sectors')
	{
		$args = array('child_of' => 21);
	}

	$categories = get_categories($args);

	$select = '<select name="filter-category">';
    $select .= '<option value="">Select...</option>';

	foreach($categories as $category)
	{
        $url = get_category_link($category->term_id);
		$select .= '<option value="' . $url . '">' . $category->name . '</option>';
	}

	$select .= '</select>';

	return $select;
}

function show_page_title()
{
	if(is_home())
	{
		$page_title = get_the_title(get_option('page_for_posts', true));
	}
    elseif(is_archive())
    {
        $page_title = single_cat_title("", false);
    }
	else
	{
		$page_title = get_the_title();
	}
	return $page_title;
}

function display_post_category_icon()
{
    if(in_category(24))
    {
        if(is_single())
        {
            $image = img_dir() . '/Garden-Centre-grey.svg';
        }
        else
        {
            $image = img_dir() . '/Garden-Centre-grey.svg';
        }
        
    }
    elseif(in_category(23))
    {
        $image = img_dir() . '/High-Street-grey.svg';
    }
    elseif(in_category(22))
    {
        $image = img_dir() . '/Public-Sector-grey.svg';
    }

    if($image)
    {
        echo '<img src="' . $image . '">';
    }
}

add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts');
add_action('wp_ajax_load_more_posts', 'load_more_posts');

function load_more_posts()
{
    $paged = $_POST['page'] + 1;
    $prev = $_POST['prev'];

    if($prev == 1 && $_POST['page'] != 1)
    {
        $paged = $_POST['page'] - 1;
    }

    $query = new WP_Query(array(
        'post_type' => 'post',
        'paged' => $paged
    ));

    if($query->have_posts())
    {

        echo '<div class="page-limit clear" data-page="/news/page/' . $paged . '">';

        while($query->have_posts())
        {
            $query->the_post();
            get_template_part('partials/post-list');
        }

        echo '</div>';

    }

    wp_reset_postdata();

    die();
}

function post_page_number($num = null)
{
    $page = '';

    if(is_paged())
    {
        $page = 'page/' . get_query_var('paged');
    }

    if($num == 1)
    {
        $paged = (get_query_var('paged') == 0 ? 1 : get_query_var('paged'));
        return $paged;
    }
    else
    {
        return $page;
    }
}

function add_first_post_class()
{
    if($i == 0 && !is_paged())
    {
        $class = 'first-post';
    }
    else
    {
        $class = '';
    }

    return $class;
}

function show_latest_news()
{
    $query = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => 3
    ));

    while($query->have_posts())
    {
        $query->the_post();
        get_template_part('partials/latest-news');
    }

    wp_reset_postdata();
}

function mail_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','mail_set_content_type' );

function send_consultation_form()
{
    $response = '';
    $error = false;

    foreach($_POST as $post)
    {
        if(empty($post))
        {
            $response = '<p class="message error">Please fill in all fields to continue.</p>';
            $error = true;
            echo $response;
            die();
        }
    }

    if(!$error)
    {
        $name = $_POST['name'];
        $company = $_POST['company'];
        $tel = $_POST['tel'];
        $email = $_POST['email'];
        $date = $_POST['date'];
        $message = 'Name: ' . $name . '<br>Company: ' . $company . '<br>Phone: ' . $tel . '<br>Email: ' . $email . '<br>Date: ' . $date;
        $to = 'jamie@icecreative.co.uk';
        $subject = 'Newspan Consultation Enquiry';
        $send_mail = wp_mail( $to, $subject, $message);

        if($send_mail)
        {
            $response = '<p class="message success">Your consultation request has been sent.</p>';
        }
        else
        {
            $response = '<p class="message error">There was a problem sending the form, please try again later.</p>';
        }
    }

    echo $response;
    die();
}
add_action( 'wp_ajax_send_consultation_form', 'send_consultation_form' );
add_action( 'wp_ajax_nopriv_send_consultation_form', 'send_consultation_form' );
