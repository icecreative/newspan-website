<?php
    require('header.php');
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
<?php get_template_part('partials/sector-icons') ?>

<div class="content-constrainer main-content">
	<h1><?php the_title(); ?></h1>
	<img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
	<?php the_content(); ?>
</div>

<?php get_template_part('partials/building-image-map') ?>

<?php get_template_part('partials/products-and-news-row') ?>

<?php get_template_part('partials/testimonial-row') ?>

<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, this page is currently unavailable.' ); ?></p>
<?php endif; ?>

<?php
    require('footer.php');
?>