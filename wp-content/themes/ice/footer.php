	<footer class="footer">
		<div class="content-constrainer">
			<h2>Contact us</h2>
			<table>
				<tr>
					<td><i class="fa fa-map-marker"></i></td>
					<td><?php the_field('address', 247) ?></td>
				</tr>
				<tr>
					<td><i class="fa fa-phone"></i></td>
					<td><a href="tel:<?php the_field('telephone', 247) ?>"><?php the_field('telephone', 247) ?></a></td>
				</tr>
				<tr>
					<td><i class="fa fa-envelope-o"></i></td>
					<td><a href="mailto:<?php the_field('email', 247) ?>"><?php the_field('email', 247) ?></a></td>
				</tr>
			</table>
			<table>
				<tr>
					<td align="left"><a href="http://facebook.com/<?php the_field('facebook', 247) ?>"><i class="fa fa-facebook"></i></a></td>
					<td width="20" align="left"><a href="http://twitter.com/<?php the_field('twitter', 247) ?>"><i class="fa fa-twitter"></i></a></td>
					<td align="left">Follow us on Facebook and Twitter</td>
					<td class="right" align="right">Newspan 2016 | Registered in England No: 06531592 | Terms and Conditions | Privacy Policy</td>
				</tr>
			</table>
			<table width="100%">
				<tr class="mobile-footer-nav">
					<td width="100%" style="width: 100%;">Newspan 2016 | Registered in England No: 06531592 | Terms and Conditions | Privacy Policy</td>
				</tr>
			</table>
		</div>
	</footer>

	</div>

	<script src="<?= components_dir() ?>/fastclick/lib/fastclick.js"></script>
	<script src="<?= components_dir() ?>/jquery/dist/jquery.js"></script>
	<script src="http://malsup.github.com/jquery.form.js"></script>
	<script src="<?= components_dir() ?>/matchHeight/dist/jquery.matchHeight.js"></script>
	<script src="<?= components_dir() ?>/remodal/dist/remodal.min.js"></script>
	<script src="<?= components_dir() ?>/slick-carousel/slick/slick.min.js"></script>
	<script src="<?= components_dir() ?>/jquery-ui/jquery-ui.js"></script>
	<script src="<?= theme_dir() ?>/main.js?v=2"></script>
	<script>
		$(document).ready(function () {
		    // Handler for .ready() called.
		    if($('#anchor').length)
		    {
		    	$('html, body').animate({
			        scrollTop: $('#anchor').offset().top -124
			    }, 500);
		    }
		});
	</script>
</body>
</html>