jQuery(document).ready(function($) {


	$( "#datepicker" ).datepicker({
		firstDay: 1,
		minDate: 0,
	    dateFormat: "d/mm/yy",
	    monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", 
                 	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        prevText: "<",
        nextText: ">",
        onSelect: function(date, picker) {
        	$('span.datepicker-date').html(date);
        	$('input.date').val(date);
        }
	});
	$( "#datepicker-mobile" ).datepicker({
		firstDay: 1,
		minDate: 0,
	    dateFormat: "d/mm/yy",
	    monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", 
                 	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        prevText: "<",
        nextText: ">",
        onSelect: function(date, picker) {
        	$('span.datepicker-date').html(date);
        	$('input.date').val(date);
        }
	});

	//Desktop submenu effect
	$('.menu-item-has-children').unbind().hover(function() {

		console.log('here');
		var subMenu = $(this).find('.sub-menu');
		subMenu.css('height', 'auto');
		subMenu.animate({
			opacity: 1
		}, 200);

	}, function() {

		var subMenu = $(this).find('.sub-menu');
		subMenu.animate({
			opacity: 0
		}, 200);
		setTimeout(function() {
			subMenu.css('height', 0);
		}, 200);

	});

	// Share button animation
	$('.share-posts').hover(function() {
		
		$('.social-icon').each(function(i) {
			var $icon = $(this);
			setTimeout(function(){
				$icon.animate({
					bottom: '6px',
					opacity: 1 
				});
			}, 100 * i);
		});

	}, function() {
		
		$('.social-icon').each(function(i) {
			var $icon = $(this);
			setTimeout(function(){
				$icon.animate({
					bottom: '44px',
					opacity: 0 
				});
			}, 100 * i);
		});

	});

	$('.map-item .title, .map-item .content').click(function(e) {
		e.stopPropagation();
		return false;
	});

	// Show/hide image map items on homepage
	$('.map-marker').on('click', function() {

		var title = $(this).siblings('.map-item-content').find('.title');
		var content = $(this).siblings('.map-item-content').find('.content');

		if($(window).width() >= 844 || $(window).width() >= '844px')
		{

			$(this).css('z-index', 52);
			$(this).siblings('.map-item-content').css('z-index', 51);

			$('.map-item-content').find('.title').each(function() {
				if($(this).hasClass('active'))
				{
					var el = $(this);

					el.parents('.map-item-content').css('z-index', 49);
					el.parents('.map-item-content').siblings('.map-marker').css('z-index', 50);

					el.siblings('.content').animate({
						height: 0
					}, 200, function() {
						el.animate({
							width: 0
						}, 200);
					});

					el.removeClass('active');
				}
			});

			
			title.addClass('active');
			var curWidth = title.width();
			title.css('width', 'auto');
			var autoWidth = title.width();

			var curHeight = content.height();
			content.css('height', 'auto');
			var autoHeight = content.height();

			title.width(curWidth);
			content.height(curHeight);

			title.animate({
				width: autoWidth
			}, 200, function() {
				content.animate({
					height: autoHeight
				}, 200);
			});

		}
		else
		{
			var item = $('[data-remodal-id=mobile-map-item]');
			item.find('.title').html(title.find('h1').html());
			item.find('.content').html(content.find('p').html());
			var inst = item.remodal();
			inst.open();
		}


	});

	/// Shrink consultation CTA on scroll
	window.setInterval(function() {
		var scrollHeight = $('header').height() - 180;
		var scrollFromTop = $(window).scrollTop();
		if(scrollFromTop >= scrollHeight)
		{
			if($('.open-consultation-service').width() == '156px' || $('.open-consultation-service').width() == 156)
			{
				$('.open-consultation-service').stop().animate({
					width: '52px'
				}, 200);
			}
		}
		else
		{
			if($('.open-consultation-service').width() == '28px' || $('.open-consultation-service').width() == 28)
			{
				$('.open-consultation-service').stop().animate({
					width: '180px'
				}, 200);
			}		
		}
	}, 500);

	// Force click caegory select boxes on change
	$('.filter select').each(function() {

		$(this).on('change', function() {
			var link = $(this).val();
			window.location.href = link;
		});
	});

	// Testimonial Carousel
	$('.testimonial-carousel').slick({
		autoPlay: true,
		autoPlaySpeed: 4000,
		infinite: true,
		fade: true,
		speed: 600,
		dots: true
	});

	// Sector Carousel
	$('.sector-carousel .carousel').slick({
		autoPlay: true,
		autoPlaySpeed: 4000,
		infinite: true,
		speed: 600,
		dots: false,
		arrows: true
	});

	// Project Carousel
	$('.project-carousel').slick({
		autoPlay: true,
		autoPlaySpeed: 4000,
		infinite: true,
		speed: 600,
		dots: false,
		arrows: true
	});

	// Featured Carousel
	$('.featured-carousel').slick({
		autoPlay: true,
		autoPlaySpeed: 4000,
		infinite: true,
		speed: 600,
		dots: false,
		arrows: true
	});

	// Staff Carousel
	$('.staff-carousel').slick({
		autoPlay: true,
		autoPlaySpeed: 4000,
		infinite: true,
		slidesToShow: 3,
		slideToScroll: 1,
		speed: 600,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 634,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

	// SLider for single case study pages
	$('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		asNavFor: '.slider-nav'
	});
	$('.slider-nav').slick({
	  	slidesToShow: 3,
	  	slidesToScroll: 1,
	  	asNavFor: '.slider-for',
	  	dots: true,
	  	focusOnSelect: true,
	  	vertical: true,
	  	arrows: true,
	  	autoplay: false
	});

	// case studyt grid hover
	$('.case-studies-grid .case-study').stop().hover(function() {
		$(this).find('.overlay').animate({
			opacity: 1
		}, 300);
	}, function() {
		$(this).find('.overlay').animate({
			opacity: 0
		}, 300);
	});

	$('.download').stop().hover(function() {
		$(this).find('.download-overlay').animate({
			opacity: 1
		}, 300);
	}, function() {
		$(this).find('.download-overlay').animate({
			opacity: 0
		}, 300);
	});

});

jQuery(window).load(function() {
	$('.post-content .title').matchHeight();
	$('.post-content').matchHeight();
	$('.service-height').matchHeight();
	$('.match-height').matchHeight();
	$('.staff-member').matchHeight();

	// News post page load transition
	display_posts();
});

var regex = /(http|ftp|https:\/\/[\w\-_]+\.{1}[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/gi;

function wrap_links_in_anchors(text) {
    return text.replace(regex,'<a href="$1$2" target="_blank">$1$2</a>');
}


// Search slides out > 1600px or popup < 1600
function search_site(el)
{
	if($(window).width() > 1600)
	{
		var searchForm = $(el).siblings('.search-form');
		var currentWidth = searchForm.width();
		searchForm.css('width', 'auto');
		var autoWidth = searchForm.width();

		if(currentWidth == 0)
		{
			searchForm.width(currentWidth).animate({
				width: autoWidth
			}, 300);

			$('.search-button').css('background', '#7fd425');
		}
		else
		{
			searchForm.submit();
		}
	}
	else
	{
		var inst = $('[data-remodal-id=mobile-search-form]').remodal();
		inst.open();
	}
}

function forceClick(el)
{
	var link = $(el).attr('href');
	document.location.href = link;
}

// Toggle the mobile menu
function toggle_mobile_menu()
{
	var mobileMenu = $('#menu-menu-1');
	console.log('Left: '+mobileMenu.position().left+', Right: '+mobileMenu.position().right);

	if(mobileMenu.position().left !== 0)
	{
		mobileMenu.animate({
			left: 0
		}, 200);
	}
	else
	{
		mobileMenu.animate({
			left: '-100%'
		}, 200);
	}
}

function submit_form(el)
{
	$(el).parents('form').submit();
}

var requestRunning = false;

function load_more_posts(event, el)
{
	event.preventDefault();

	if(requestRunning)
	{
		return;
	}

	var page = parseInt($(el).attr('data-page'));
	var ajaxurl = $(el).attr('data-url');
	var scrollPosition = parseInt($('.post-page .posts .post').last().offset().top);
	var scrollOffset = parseInt($('.post-page .posts .post').last().height());
	var prev = $(el).attr('data-prev');

	if(typeof prev === 'undefined')
	{
		prev = 0;
	}

	$('.ajax-loader i').addClass('fa-spin');
	$(el).children('.button-text').html('Loading');
	
	$.ajax({
		url: ajaxurl,
		type: 'post',
		data: {
			page: page,
			prev: prev,
			action: 'load_more_posts'
		},
		error: function(response) {
			console.log(response);
		},
		success: function(response) {
			var delay = 500;

			$(el).attr('data-page', (page + 1));

			setTimeout(function() {

				$('.ajax-loader i').removeClass('fa-spin');
				$('.post-content .title').matchHeight();
				$('.post-content').matchHeight();
				
				if(prev == 1)
				{
					console.log(page);
					$('.posts').prepend(response);
					$(window).scrollTop();
				}
				else
				{
					$('.posts').append(response);
					$(window).scrollTop(scrollPosition + scrollOffset);
				}
				
				display_posts();

				if($(el).children('.button-text').hasClass('previous'))
				{
					$(el).children('.button-text').html('Load previous');
				}
				else
				{
					$(el).children('.button-text').html('Load more');
				}
				
			}, delay);
		},
		complete: function() {
			requestRunning = false;
		}
	});

	requestRunning = true;
	return false;
}

var lastScroll = 0;

$(window).scroll(function() {

	var scroll = $(window).scrollTop();

	if(Math.abs(scroll - lastScroll) > $(window).height() * 0.1)
	{
		lastScroll = scroll;
		$('.page-limit').each(function(i) {
			if(isVisible($(this)))
			{
				history.replaceState(null, null, $(this).attr('data-page'));
				return false;
			}
		});
	}
});

function display_posts()
{
	var iterator = 0;

	$('.post-page .posts .post').each(function(i, el) {
		if(!$(el).hasClass('visible'))
		{
			setTimeout(function(){
	       		$(el).addClass('visible');
	    	},( iterator * 400 ));
	    	iterator += 1;
		}
	});
}

function isVisible(el)
{
	var scrollPosition = $(window).scrollTop();
	var windowHeight = $(window).height();
	var elementOffsetTop = $(el).offset().top;
	var elementHeight = $(el).height();
	var elementOffsetBottom = elementOffsetTop + elementHeight;

	return ((elementOffsetBottom - elementHeight * 0.25 > scrollPosition) && (elementOffsetTop < (scrollPosition + 0.5 *windowHeight)));
}

function open_consultation_service(el)
{
	if($(el).css('right') == '0px' || $(el).css('right') == 0)
	{
		$(el).animate({
			right: '418px'
		}, 300);

		$(el).find('.fa').removeClass('fa-angle-left').addClass('fa-angle-right');

		$('.consultation-service').animate({
			right: '0px'
		}, 300);
	}
	else
	{
		$(el).animate({
			right: '0px'
		}, 300);

		$(el).find('.fa').removeClass('fa-angle-right').addClass('fa-angle-left');

		$('.consultation-service').animate({
			right: '-418px'
		}, 300);
	}
}

function show_mobile_consultation(el)
{
	var content = $(el).siblings('.consultation-content');
	$(el).animate({
		paddingBottom: '40px'
	}, 400);

	var curHeight = content.height();
	content.css({
		height: 'auto',
		overflow: 'visible'
	});
	var autoHeight = content.height() + 40;

	console.log(autoHeight);

	setTimeout(function() {
		content.height(curHeight).animate({
			height: autoHeight,
			opacity: 1
		}, 400, function(){
			content.find('.icon-image').animate({
				opacity: 1
			}, 400);
		});
	}, 400);
}

function submit_consultation_form(event, el)
{
	event.preventDefault();
	
	var error = false;
	var message;
	var form_data = {};

	$(el).find('input').each(function() {
		if($(this).val() !== '')
		{
			form_data[this.name] = $(this).val();
		}
		else
		{
			message = 'Please fill in all fields to submit the form.';
			error = true;
		}
	});

	if(!error)
	{
		$(el).ajaxSubmit({
			url: ajax_url,
			data: form_data,
			type: 'POST',
			success: function(response) {
				$('p.message').replaceWith(response);
			}
		});
	}
	else
	{
		$('p.message').addClass('error').html(message);
	}

}

function caseStudyArrowClick(el)
{
	var current = $('.slides .image.active');
	var arrow = $(el);

	current.removeClass('active');

	if(arrow.hasClass('left-arrow'))
	{
		if(current.prev().length)
		{
			current.prev().addClass('active');
		}
		else
		{
			$('.slides .image').last().addClass('active');
		}
	}
	else
	{
		if(current.next().length)
		{
			current.next().addClass('active');
		}
		else
		{
			$('.slides .image').first().addClass('active');
		}
	}
}

function validate_form(event, el)
{

	if($(el).find('.search-field').val() == '')
	{
		$('.error').html('Please provide a search term');
		event.preventDefault();
		return false;
	}
	
}