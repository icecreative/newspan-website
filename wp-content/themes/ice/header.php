<!DOCTYPE html>
<!--[if lte IE 7]>      <html lang="en-GB" class="nojs lte-ie9 lte-ie8 lte-ie7"> <![endif]-->
<!--[if lte IE 8]>      <html lang="en-GB" class="nojs lte-ie9 lte-ie8"> <![endif]-->
<!--[if IE 9]>          <html lang="en-GB" class="nojs lte-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html lang="en-GB" class="nojs"> <!--<![endif]-->
<head>

    <title>Newspan</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
	    <script src="<?= components_dir() ?>/html5shiv/dist/html5shiv.js"></script>
	<![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?= components_dir() ?>/normalize.css/normalize.css">
    <link rel="stylesheet" href="<?= components_dir() ?>/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?= components_dir() ?>/animate.css/animate.css">
    <link rel="stylesheet" href="<?= components_dir() ?>/remodal/dist/remodal.css">
    <link rel="stylesheet" href="<?= components_dir() ?>/remodal/dist/remodal-default-theme.css">
    <link rel="stylesheet" href="<?= components_dir() ?>/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="<?= components_dir() ?>/slick-carousel/slick/slick-theme.css">
	<link rel="stylesheet" href="<?= theme_dir() ?>/style.css?v=2">
    <script src="<?= components_dir() ?>/modernizr/modernizr.js"></script>
    <script src="<?= components_dir() ?>/detectizr/dist/detectizr.js"></script>
</head>
<body>
<div class="site">
<!-- Facebook API -->
<script>
    window.fbAsyncInit = function() {
        var token = '528320460685158|NK7QH1Pp-JqT8QjqHO7hYaGIZMI';
        FB.init({
            appId      : 528320460685158,
            xfbml      : true,
            version    : 'v2.7'
        });
        FB.api(
            "/newspan/posts?limit=3",
            function (response) {
                if (response && !response.error) {

                    var data = '<ul>';

                    for(i = 0; i < response.data.length; i++)
                    {
                        var date = new Date(response.data[i].created_time);
                        var seconds = Math.floor((new Date() - date) / 1000);
                        var interval = Math.floor(seconds / 86400);
                        data += '<li>' + response.data[i].message + '<br><span>' + interval + ' days ago</span></li>';
                    }

                    data += '</ul>';
                    $('.facebook-posts').append(wrap_links_in_anchors(data));

                }
                else
                {
                    console.log(response);
                    $('.facebook-posts').append('Our Facebook posts are currently unavailable, please visit our page <a href="https://www.facebook.com/newspan" target="_blank">here</a> to see our posts.');
                }
            }, { access_token: token }
        );
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- // Facebook API -->

<?php get_template_part('partials/remodals') ?>

<?php get_template_part('partials/header-content') ?>


<?php if(!is_front_page()) : ?>
    <div class="content-constrainer main-content page-header">
        <h1><?= show_page_title() ?></h1>
        <img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
        <?php if(is_home()) : ?>
            <?php the_field('content', 34) ?>
        <?php endif; ?>
    </div>
<?php endif; ?>

