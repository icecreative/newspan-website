<?php
/**
 * Template Name: Projects Template
 */
?>

<?php
    get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="content-constrainer main-content">
	<?php the_content() ?>
</div>

<div class="projects-page">
	<?php get_template_part('partials/case-studies-grid') ?>
</div>

<?php endwhile; else : ?>

 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

<?php endif; ?>
<?php
    get_footer();
?>