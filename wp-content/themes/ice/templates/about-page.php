<?php
/**
 * Template Name: About Page Template
 */
?>

<?php
    get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="content-constrainer main-content">
	<?php the_content() ?>
</div>

<div class="mission-statement">
	<div class="content-constrainer">
		<h1>MISSION STATEMENT</h1>
        <img class="splitter" src="<?= img_dir() ?>/splitterr.png" alt="">
		<?php the_field('mission_statement') ?>	
	</div>
</div>

<div class="content-constrainer main-content team-section">
	<h2><?php the_field('before_team_title') ?></h2>
	<img class="splitter" src="<?= img_dir() ?>/green-splitter.png" alt="">
	<p><?php the_field('before_team_content') ?></p>
	<?php get_template_part('partials/staff-row') ?>
	<h2><?php the_field('after_team_title') ?></h2>
	<img class="splitter" src="<?= img_dir() ?>/green-splitter.png" alt="">
	<p><?php the_field('after_team_content') ?></p>
</div>

<?php get_template_part('partials/testimonial-row') ?>

<?php get_template_part('partials/sustainable-building-row') ?>

<?php endwhile; else : ?>

 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

<?php endif; ?>
<?php
    get_footer();
?>