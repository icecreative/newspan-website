<?php
/**
 * Template Name: Downloads Template
 */
?>

<?php
    get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="content-constrainer main-content">
	<?php the_content() ?>
</div>

<div class="content-constrainer downloads clear">
	<?php if(have_rows('download')) : while(have_rows('download')) : the_row('download'); ?>
		<div class="download">
			<img src="<?= get_sub_field('image')['url'] ?>" alt="">
			<div class="download-overlay">
				<h1><?php the_sub_field('title') ?></h1>
				<p>hello world</p>
			</div>
			<div class="button">
				<a href="<?= get_sub_field('file')['url'] ?>" class="btn">DOWNLOAD</a>
			</div>
		</div>
	<?php endwhile; endif; ?>
</div>


<?php endwhile; else : ?>

 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

<?php endif; ?>
<?php
    get_footer();
?>