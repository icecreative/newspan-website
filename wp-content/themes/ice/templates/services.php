<?php
/**
 * Template Name: Services Template
 */
?>

<?php
    get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="content-constrainer main-content">
	<?php the_content() ?>
</div>

<?php get_template_part('partials/projects-content') ?>
<?php get_template_part('partials/case-studies-grid') ?>

<?php endwhile; else : ?>

 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

<?php endif; ?>
<?php
    get_footer();
?>