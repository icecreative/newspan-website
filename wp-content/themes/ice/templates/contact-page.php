<?php
/**
 * Template Name: Contact Template
 */
?>

<?php
    get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="content-constrainer main-content">
	<?php the_content() ?>

	<div class="map">
		<img src="<?= img_dir() ?>/map.png" alt="">
		<div class="overlay">
			<h2><?php the_field('map_title') ?></h2>
			<p><?php the_field('map_content') ?></p>
		</div>
	</div>
</div>

<?php get_template_part('partials/contact-section') ?>

<?php endwhile; else : ?>

 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

<?php endif; ?>
<?php
    get_footer();
?>